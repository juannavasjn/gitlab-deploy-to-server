const express = require('express');
const bodyParser = require('body-parser');
const app = express();

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

app.get('/', (req, res) => {
	res.json('Gitlab - CI - Hey Juan! 3 -' + new Date().toLocaleString());
});

process.env.PORT = 3000;

app.listen(process.env.PORT, () => {
	const log = 'Open https://localhost:' + process.env.PORT + ' - ' + new Date().toLocaleString();
	console.log(log);
});
